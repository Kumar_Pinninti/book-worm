// load book from catelog/disk
function loadBook(filename, displayName){
    console.log(filename, displayName)
    let currentBook = '';
    let url = "assets/" + filename;

    //reset UI
    document.getElementById('fileName').innerHTML = displayName;
    document.getElementById('searchStat').innerHTML = '';
    document.getElementById('keyword').value = '';

    // create a server request to load the book
    var xhr = new XMLHttpRequest();
    xhr.open("GET", url, true);
    xhr.send();

    xhr.onreadystatechange = function(){
        if(xhr.readyState == 4 && xhr.status == 200){
            currentBook = xhr.responseText;

            getDocStats(currentBook);

            //remove line breaks and carriage returns and replace with br tag
            currentBook = currentBook.replace(/(?:\r\n\|\r|\n)/g, '<br>');

            document.getElementById('fileContent').innerHTML = currentBook;

            var ele = document.getElementById('fileContent');
            ele.scrollTop = 0;
        }
    };
}


//get the stats from the book
function getDocStats(fileContent){

    var docLength = document.getElementById('docLength');
    var wordCount = document.getElementById('wordCount');
    var charCount = document.getElementById('charCount');

    let text = fileContent.toLowerCase();
    let wordArray = text.match(/\b\S+\b/g);
    let wordDictionary = {};

    var unCommonWords = []

    // fliter function
    unCommonWords = filterStopWords(wordArray);

    //count every word in the wordArray
    for(let word in unCommonWords){
        let wordValue = unCommonWords[word];
        if(wordDictionary[wordValue] > 0){
            wordDictionary[wordValue] += 1;
        }
        else{
            wordDictionary[wordValue] = 1;
        }
    }

    // sort the array
    let wordList = sortProperties(wordDictionary);

    //return top 5 used words
    var top5words = wordList.slice(0,6);

    //return least 5 used words
    var least5words = wordList.slice(-6, wordList.length);

    //write the values to the page
    ULTemplate(top5words, document.getElementById('mostUsed'));
    ULTemplate(least5words, document.getElementById('leastUsed'));

    docLength.innerText = "Document Length: " + text.length;
    wordCount.innerText = "Word(s) Count: " + wordArray.length;
}


function ULTemplate(items, element){
    let rowTemplate = document.getElementById('template-ul-items');
    let templateHTML = rowTemplate.innerHTML;
    let resultsHTML = '';

    for(i=0; i<items.length; i++){
        resultsHTML += templateHTML.replace('{{val}}', items[i][0] + " : " + items[i][1] + " time(s)");
    }

    element.innerHTML = resultsHTML;
}

function sortProperties(obj){
    //covert the obj into array
    let rtnArray = Object.defineProperties(obj);

    //sort the array
    rtnArray.sort(function(first, second){
        return second[1] - first[1]; // [1] is bcoz, it is array in array we are sorting based on wordsvalue
    });

    return rtnArray;
}

//filtering uncommon words
function filterStopWords(wordArray){
    var commonWords = getStopWords();
    var commonObj = {};
    var unCommonArr = [];

    for(i=0; i<commonWords.length; i++){
        commonObj[commonWords[i].trim()] = true;
    }

    for(i=0; i<wordArray.length; i++){
        word = wordArray[i].trim().toLowerCase();
        if(!commonObj[word]){
            unCommonArr.push(word);
        }
    }

    return unCommonArr;
}


// a list of stop words we dont want to include
function getStopWords(){
    return ["a", "able", "about", "across", "of", "he", "she", "the"];
}


//perform search to mark words after search
function performMark(){

    //read the keyword
    var keyword = document.getElementById('keyword').value;
    var display = document.getElementById('fileContent');

    var newContent = "";

    //find all currently marked items
    let spans = document.querySelectorAll('mark');

    //<mark>Harry</mark> to Harry
    for(var i = 0; i < spans.length; i++){
        spans[i].outerHTML = spans[i].innerHTML;
    }

    var re = new RegExp(keyword, "gi");
    var replaceText = '<mark id="markme">$&</mark>';
    var bookContent = display.innerHTML;

    //add the mark to the book content
    newContent = bookContent.replace(re, replaceText);

    display.innerHTML = newContent;
    var count = document.querySelectorAll('mark').length;
    document.getElementById("searchStat").innerHTML = "found " + count + " matches";

    if(count > 0){
        var element = document.getElementById('markme');
        element.scrollIntoView();
    };
    
}